import sqlite3

conn = sqlite3.connect('DataBase.db')
c = conn.cursor()

# Create table - USERS
c.execute('''CREATE TABLE USERS
            ([user_id] INTEGER PRIMARY KEY,
            [users] text,
            [created] date)''')

# Create table - FORUMS
c.execute('''CREATE TABLE FORUMS
            ([forum_id] INTEGER PRIMARY KEY,
            [forum] text)''')

# Create table - TOPICS
c.execute('''CREATE TABLE TOPICS
            ([topic_id] INTEGER PRIMARY KEY,
            [topic] text,
            [forum_id] integer,
            FOREIGN KEY ([forum_id]) REFERENCES FORUMS ([forum_id]))''')

# Create table - POSTS
c.execute('''CREATE TABLE POSTS 
          ([post_id] INTEGER PRIMARY KEY,
          [post_value] text,
          [topic_id] integer,
          [user_id] integer,          
          FOREIGN KEY ([topic_id]) REFERENCES TOPICS ([topic_id]),
          FOREIGN KEY ([user_id]) REFERENCES USERS ([user_id]))''')

conn.commit()
